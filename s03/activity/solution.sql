USE blog_db;

-- Show table users.
DESCRIBE users;

--Add Following users
INSERT INTO users (email, password, datetime_created) VALUES 
("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"),
("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00"),
("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"),
("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"),
("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

--Check Users Table;

SELECT * FROM users;

-- Add following posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES
(1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
(1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
(2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
(4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- Check data from posts
SELECT * FROM posts;

-- GET ALL THE POST WITH AN AUTHOR ID OF 1

SELECT title, content, datetime_posted FROM posts WHERE author_id = 1;

-- GET ALL USER'S EMAIIL AND DATETIME OF CREATION

SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete the user with an email of "johndoe@gmail.com".
DELETE FROM users WHERE email = "johndoe@gmail.com";


-- STRETCH GOALS

-- Insert the following records in the music_db database

DESCRIBE users;

--USERS
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES
("johndoe", "john1234", "John Doe", "09123456789", "john@mail.com", "Quezon City");

-- CHECK TABLE IIF RECORD IS ADDED
SELECT * FROM users;

--PLAYLIST

DESCRIBE playlists;

--ADd playlist
INSERT INTO playlists (datetime_created, user_id) VALUES ("2023-09-20 08:00:00", 1);

-- CHECK IF RECORD IS ADDED
SELECT * FROM playlists;

--ADD SONG TO PLAYLISTS

DESCRIBE playlists_songs;

-- Add songs to playlist;
INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1,1),(1,2);

-- CHECK IF RECORD IS ADDED

SELECT * FROM playlists_songs;